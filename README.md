# What is Prosody?

Prosody is a cross-platform XMPP server written in Lua.

# How to use this image

## Running Prosody

This Dockerfile will automatically start Prosody using default settings and expose client to server (c2s) on port 5222 and server to server (s2s) on port 5269.

	sudo docker run -p 5222:5222 -p 5269:5269 -d simplyintricate/prosody

To help maintain immutability of the image, you may extend this image to include your own configurations and make that into your own version of Prosody. Here is a sample Dockerfile for that

	FROM simplyintricate/prosody

	ADD prosody.cfg.lua /etc/prosody/prosody.cfg.lua

## Contributing

You can help make this Dockerfile better by contributing at [Github](https://github.com/stephenliang/mediawiki-dockerfile)

If you found this Docker image helpful, send a tip via Bitcoin to 14b9y1Qw17coEkJFaAAvuXpKZLadTeBPw7

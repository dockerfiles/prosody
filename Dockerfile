FROM debian:jessie

RUN apt-get update && \
	apt-get install -y wget ca-certificates

RUN echo deb http://packages.prosody.im/debian jessie main | tee -a /etc/apt/sources.list && \
	wget https://prosody.im/files/prosody-debian-packages.key -O- | apt-key add - && \
	apt-get update && \
	apt-get install -y prosody-0.10 && \
	sed -i '1s/^/daemonize = false;\n/' /etc/prosody/prosody.cfg.lua && \
	perl -i -pe 'BEGIN{undef $/;} s/^log = {.*?^}$/log = {\n    {levels = {min = "info"}, to = "console"};\n}/smg' /etc/prosody/prosody.cfg.lua && \
	apt-get remove -y wget && \
	apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER prosody
EXPOSE 5222 5269
VOLUME /var/lib/prosody

CMD ["/usr/bin/prosodyctl", "start"]
